<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/searchScript.js","","",10);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.css");
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
});





// Remove Emoji
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'gallery-menu' => __( 'Gallery Menu' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


//* Remove Query String from Static Resources
function remove_css_js_ver( $src ) {
    if( strpos( $src, '?ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'remove_css_js_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_css_js_ver', 10, 2 );


// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

//Facet Title Hook
add_filter('facetwp_shortcode_html', function ($output, $atts) {
    if (isset($atts['facet'])) {
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2);


/* ACF add options page */
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page( array(

        'page_title'    => 'Theme Settings',
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-settings',
        'capability'    => 'edit_posts',
        'icon_url' => 'dashicons-images-alt2',
        'position' => 65

    ) );

}

// Facetwp results
add_filter( 'facetwp_result_count', function( $output, $params ) {
    //$output = $params['lower'] . '-' . $params['upper'] . ' of ' . $params['total'] . ' results';
    $output =  number_format($params['total']) . ' Products';
    return $output;
}, 10, 2 );

// Facetwp results pager
function my_facetwp_pager_html( $output, $params ) {
    $output = '';
    $page = $params['page'];
    $total_pages = $params['total_pages'];
    if ( $page > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page - 1) . '"><span class="pager-arrow"><</span></a>';
    }
    $output .= '<span class="pager-text">page ' . $page . ' of ' . $total_pages . '</span>';
    if ( $page < $total_pages && $total_pages > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page + 1) . '"><span class="pager-arrow">></span></a>';
    }
    return $output;
}

add_filter( 'facetwp_pager_html', 'my_facetwp_pager_html', 10, 2 );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');

//Gravity Scroll to confirmation
add_filter( 'gform_confirmation_anchor', '__return_true' );



function cptui_register_my_cpts_glass_tile() {

    /**
     * Post Type: Glass Tile.
     */

    $labels = array(
        "name" => __( 'Glass Tile', '' ),
        "singular_name" => __( 'Glass Tile', '' ),
        "menu_name" => __( 'Glass Tile Catalog', '' ),
    );

    $args = array(
        "label" => __( 'Glass Tile', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "flooring/glass-tile/glass-tile-catalog", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title" ),
    );

    register_post_type( "glass_tile", $args );
}

add_action( 'init', 'cptui_register_my_cpts_glass_tile' );



function cptui_register_my_cpts_natural_stone() {

    /**
     * Post Type: Natural Stone.
     */

    $labels = array(
        "name" => __( 'Natural Stone', '' ),
        "singular_name" => __( 'Natural Stone', '' ),
        "menu_name" => __( 'Natural Stone Catalog', '' ),
    );

    $args = array(
        "label" => __( 'Natural Stone', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "flooring/natural-stone/natural-stone-catalog", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title" ),
    );

    register_post_type( "natural_stone", $args );
}

add_action( 'init', 'cptui_register_my_cpts_natural_stone' );



//Automatically registers new BB Modules from the theme
function register_custom_modules(){
    if ( class_exists( 'FLBuilder' ) ) {
        $dir=dirname(__FILE__)."/fl-builder/custom-modules";
        $files=scandir($dir);
        foreach($files as $k=>$v){
            $file=$dir."/".$v;
            if(is_dir($file) && file_exists($file."/index.php")){
                include_once($file."/index.php");
            }
        }
    }
}

//add_action("init","register_custom_modules");

add_image_size("logo_slider",false,52);
function new_year_number() 
{
	return $new_year = date('Y');
}
add_shortcode('year_code_4', 'new_year_number');

function new_year_number_2() 
{
	return $new_year = date('y');
}
add_shortcode('year_code_2', 'new_year_number_2');


/* Career */
function career_display()
{

    $career_html = '<div class="table-responsive"><table class="responsive careersTable table table-bordered" style="border: 1px solid #efefef; width: 100%; padding-top: 10px; padding-bottom: 10px;">
    <tbody>
    <tr style="padding-top: 5px; padding-bottom: 5px;">
    <th>Position</th>
    <th>Type</th>
    <th class="tableDescription">Description</th>
    <th>Location</th>
    <th>Action</th>
    </tr>';
    $args = array(  
        'post_type' => 'career_type',
        'post_status' => 'publish',
        
    );

    $loop = new WP_Query( $args ); 
        
    while ( $loop->have_posts() ) : $loop->the_post(); 
    $career_html .= '<tr style="font-size: 11px; padding-top: 10px; padding-bottom: 10px; background-color: #efefef;" data-career="'.get_the_title().'" >
    <td>'.get_the_title().'</td>
    <td>'.get_field('job_type').'</td>
    <td class="tableDescription">'.get_the_content().'</td>
    <td>'.get_field('job_location').'</td>
    <td><a href="javascript:void(0);" class="applyposiiton"  data-title="'.get_the_title().'" onclick="positionAppend(\''.get_the_title().'\')">Apply Now</a></td>
    </tr>';
    endwhile;

    wp_reset_postdata(); 
    
    
    $career_html .= '</tbody>
    </table> </div>';
    return $career_html;

} 
add_shortcode('careers_shortcode', 'career_display');



// //Yoast SEO Breadcrumb link - Changes for plp pages only

remove_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail' );

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_cusrom_override_yoast_breadcrumb_trail' );

function wpse_cusrom_override_yoast_breadcrumb_trail( $links ) {
    global $post;

    if ( is_singular( 'hardwood_catalog' )  ) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood/',
            'text' => 'Hardwood',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood/hardwood-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );

    }
     if (is_singular( 'carpeting' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpet/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpet/carpet-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );

    }
     if (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/luxury-vinyl-tile/',
            'text' => 'Luxury Vinyl Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/luxury-vinyl-tile/luxury-vinyl-tile-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }
     if (is_singular( 'laminate_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/',
            'text' => 'Laminate',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/laminate-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }
     if (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/ceramic-porcelain-tile/',
            'text' => 'Ceramic & Porcelain Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/ceramic-porcelain-tile/ceramic-porcelain-tile-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }
     if (is_singular( 'glass_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/glass-tile/',
            'text' => 'Glass Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/glass-tile/glass-tile-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
    }

    if (is_singular( 'glass_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/glass-tile/',
            'text' => 'Glass Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/glass-tile/glass-tile-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
    }
    

    return $links;
}



// function that runs Logo Shortcode
function wpb_demo_shortcode() { 
 
    if (is_page( 'commercial' )) {
        $logo = '<a href="https://hadingerflooring.com/"><img src="https://hadingerflooring.com/wp-content/uploads/2018/08/commercial-1.png" class="headerImg"/></a>';
    }else if (is_page( 'area-rugs' )) {
        $logo = '<a href="https://hadingerflooring.com/"><img src="https://hadingerflooring.com/wp-content/uploads/2017/08/logo-hadinger-rug.jpg" class="headerImg"/></a>';
    }else if (is_page( 'cabinets' )) {
        $logo = '<a href="https://hadingerflooring.com/"><img src="https://hadingerflooring.com/wp-content/uploads/2017/08/logo-hadinger-cabinets.png" class="headerImg"/></a>';
    }else {
        $logo = '<a href="https://hadingerflooring.com/"><img src="https://hadingerflooring.com/wp-content/uploads/2017/06/logo-hadinger.png" class="headerImg"/></a>'; 
    }
    


// Output needs to be return
return $logo;
} 
// register shortcode
add_shortcode('ShowLogo', 'wpb_demo_shortcode'); 


//301 redirect added from 404 logs table
wp_clear_scheduled_hook( '404_redirection_log_cronjob' );
wp_clear_scheduled_hook( '404_redirection_301_log_cronjob' );
if (!wp_next_scheduled('hadingerflooring_404_redirection_301_log_cronjob')) {
    
    $interval =  getIntervalTime('friday');    
    wp_schedule_event( time() +  17800, 'daily', 'hadingerflooring_404_redirection_301_log_cronjob');
}

add_action( 'hadingerflooring_404_redirection_301_log_cronjob', 'hadingerflooring_custom_404_redirect_hook' ); 

function custom_check_404($url) {
   $headers=get_headers($url, 1);
  if ($headers[0]!='HTTP/1.1 200 OK') {return true; }else{ return false;}
}

 // custom 301 redirects from  404 logs table
 function hadingerflooring_custom_404_redirect_hook(){
    global $wpdb;    
    write_log('in function');

    $table_redirect = $wpdb->prefix.'redirection_items';
    $table_name = $wpdb->prefix . "redirection_404";
    $table_group = $wpdb->prefix.'redirection_groups';

     $data_404 = $wpdb->get_results( "SELECT * FROM $table_name" );
     $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
     $redirect_group =  $datum[0]->id;  

    if ($data_404)
    {      
      foreach ($data_404 as $row_404) 
       {            

        if (strpos($row_404->url,'carpet') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {
            write_log($row_404->url);      
            
           $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = custom_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $source_url = $row_404->url;
            $destination_url = '/flooring/carpet/carpet-catalog/';
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $source_url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            write_log( 'carpet 301 added ');
         }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');


         }

        }
        else if (strpos($row_404->url,'hardwood') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');
            $url_404 = home_url().''.$row_404->url;
            $headers_res = custom_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $source_url = $row_404->url;
            $destination_url = '/flooring/hardwood/hardwood-catalog/';
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $source_url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }

            write_log( 'hardwood 301 added ');

        }
        else if (strpos($row_404->url,'laminate') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = custom_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $source_url = $row_404->url;
            $destination_url = '/flooring/laminate/laminate-catalog/';
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $source_url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'laminate 301 added ');


        }
        else if ((strpos($row_404->url,'luxury-vinyl') !== false || strpos($row_404->url,'vinyl') !== false) && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);  

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = custom_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $source_url = $row_404->url;
            if (strpos($row_404->url,'luxury-vinyl') !== false ){
                $destination_url = '/flooring/luxury-vinyl-tile/luxury-vinyl-tile-catalog/';
            }elseif(strpos($row_404->url,'vinyl') !== false){
                $destination_url = '/flooring/luxury-vinyl-tile/luxury-vinyl-tile-catalog/';
            }
            
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $source_url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
            }

            write_log( 'luxury-vinyl 301 added ');
        }
        else if (strpos($row_404->url,'tile') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = custom_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $destination_url = '/flooring/';
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $source_url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'tile 301 added ');
        }

       }  
    }

 }

function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js", array(), '3.2.1' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
   // write_log($sql_delete);	
}
 add_filter( 'auto_update_plugin', '__return_false' );