<?php
//$show_thumbs = FLTheme::get_setting('fl-posts-show-thumbs');
?>
<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/Product">

	<header class="fl-post-header">

		<?php //FLTheme::post_top_meta(); ?>
	</header><!-- .fl-post-header -->

	<div class="fl-post-content clearfix grey-back" itemprop="text">

        <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 product-swatch">

<?php 
 $image = get_field('swatch_image_link');$height ='600';$width ='600';
 if(strpos($image, 'mmllc-images.s3') !== false || strpos($image, 'products.daltile.com') !== false || strpos($image, 's7.shawimg.com') !== false || strpos($image, 's7.shawfloors.com') !== false || strpos($image, 's7d4.scene7.com') !== false){

        
    if(strpos($image , 's7.shawimg.com') !== false){
        if(strpos($image , 'http') === false){ 
        $image = "http://" . $image;
    }	
       
    }else{
        if(strpos($image , 'http') === false){ 
        $image = "https://" . $image;
    }	
        $image= "https://mm-media-res.cloudinary.com/image/fetch/h_".$height.",w_".$width.",c_limit/".$image."";
    }

  }else{

    $image= 'https://res.cloudinary.com/mobilemarketing/image/upload/c_limit,h_'.$height.',w_'.$width.',q_auto'.$image;

  }	

   $image_room = get_field('room_scene_image_link');$height ='600';$width ='600';
 if(strpos($image_room, 'mmllc-images.s3') !== false || strpos($image_room, 'products.daltile.com') !== false || strpos($image_room, 's7.shawimg.com') !== false || strpos($image_room, 's7.shawfloors.com') !== false || strpos($image_room, 's7d4.scene7.com') !== false){

        
    if(strpos($image_room , 's7.shawimg.com') !== false){
        if(strpos($image_room , 'http') === false){ 
        $image_room = "http://" . $image_room;
    }	
       
    }else{
        if(strpos($image_room , 'http') === false){ 
        $image_room = "https://" . $image_room;
    }	
        $image_room= "https://mm-media-res.cloudinary.com/image/fetch/h_".$height.",w_".$width.",c_limit/".$image_room."";
    }

  }else{

    $image_room= 'https://res.cloudinary.com/mobilemarketing/image/upload/c_limit,h_'.$height.',w_'.$width.',q_auto'.$image_room;

  }	
?>
                <?php if (get_field('brand') == 'Quick-Step' || get_field('brand') == 'Daltile') { ?>

                    <div class="img-responsive toggle-image" style="background-image:url('<?php the_field('swatch_image_link'); ?>');background-size: cover;">
                        <img src="<?php the_field('swatch_image_link'); ?>" class="img-responsive toggle-image" />
                    </div>
                    <?php if(get_field('room_scene_image_link')){ ?>
                        <div class="toggle-image-thumbnails">
                            <?php
                            $a=array();
                            $a[]=$image;
                            $a[]=$image_room;

                            foreach($a as $k=>$v){
                                ?><a href="#" data-background="<?php echo $v ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $v ?>');background-size: cover;"></a><?php
                            }
                            ?>
                        </div>
                    <?php } ?>

                <?php } else { ?>

                    <div class="img-responsive toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: cover;">
                        <img src="<?php echo $image; ?> " class="img-responsive toggle-image" />
                    </div>
                    <?php if(get_field('room_scene_image_link')){ ?>
                        <div class="toggle-image-thumbnails">
                            <?php
                            $a=array();
                            $a[]=$image;
                            $a[]=$image_room;

                            foreach($a as $k=>$v){
                                ?><a href="#" data-background="<?php echo $v; ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $v; ?>');background-size: cover;"></a><?php
                            }
                            ?>
                        </div>
                    <?php } ?>

                <?php } ?>


            </div>
            <div class="col-md-6 col-sm-6 product-box">

                <?php get_template_part('includes/product-brand-logos'); ?>

                <?php if(get_field('parent_collection')) { ?>
                <h2><?php the_field('parent_collection'); ?> </h2>
                <?php } ?>


                <h2><?php the_field('collection'); ?></h2>
                <h1 class="fl-post-title" itemprop="name">
                   <?php the_field('color'); ?>
                </h1>

                <div class="product-colors">
                    <?php
                    $familysku = get_post_meta($post->ID, 'collection', true);
                    if(is_singular( 'laminate' )){
                        $flooringtype = 'laminate';
                    } elseif(is_singular( 'hardwood' )){
                        $flooringtype = 'hardwood';
                    } elseif(is_singular( 'carpeting' )){
                        $flooringtype = 'carpeting';
                    } elseif(is_singular( 'luxury_vinyl_tile' )){
                        $flooringtype = 'luxury_vinyl_tile';
                    } elseif(is_singular( 'vinyl' )){
                        $flooringtype = 'vinyl';
                    } elseif(is_singular( 'solid_wpc_waterproof' )){
                        $flooringtype = 'solid_wpc_waterproof';
                    } elseif(is_singular( 'tile' )){
                        $flooringtype = 'tile';
                    } elseif(is_singular( 'glass_tile' )){
                        $flooringtype = 'glass_tile';
                    } elseif(is_singular( 'natural_stone' )){
                        $flooringtype = 'natural_stone';
                    }

                    $args = array(
                        'post_type'      => $flooringtype,
                        'posts_per_page' => -1,
                        'post_status'    => 'publish',
                        'meta_query'     => array(
                            array(
                                'key'     => 'collection',
                                'value'   => $familysku,
                                'compare' => '='
                            )
                        )
                    );
                    ?>
                    <?php
                    $the_query = new WP_Query( $args );
                    ?>
                    <ul>
                        <li class="found"><?php  echo $the_query ->found_posts; ?></li>


                        <li class="colors">Colors<br/>Available</li>
                    </ul>

                </div>

                <a href="<?php echo site_url(); ?>/coupon/" class="fl-button" role="button" style="width: auto;">
                    <span class="fl-button-text">GET COUPON</span>
                </a>
<br />
                <a href="<?php echo get_page_link(15); ?>" class="fl-button btn-white" role="button" style="width: auto;">
                    <span class="fl-button-text">CONTACT FOR INFO</span>
                </a>
                <br />
                <a href="<?php echo get_page_link(19); ?>" class="estimate-link">FREE IN-HOME ESTIMATE ></a>

                <br />

                <?php  roomvo_script_integration(get_field('brand'), get_field('sku')); ?>

                <div class="product-atts">

                </div>
            </div>
        </div>


        <?php get_template_part('includes/product-color-slider'); ?>
</div>



    </div><!-- .fl-post-content -->
<div class="container">
    <?php get_template_part('includes/product-attributes'); ?>
</div>

	<?php //FLTheme::post_bottom_meta(); ?>
	<?php //FLTheme::post_navigation(); ?>
	<?php //comments_template(); ?>

</article>
<!-- .fl-post -->