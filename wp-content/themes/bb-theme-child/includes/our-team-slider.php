

<div class="our-team">
    <div class="row">
        <?php
        $postid = get_the_ID();

        $args = array(
            'post_type'      => 'page',
            'posts_per_page' => -1,
            'post_status'    => 'publish',
            'page_id' => $postid,
        );
        ?>
        <?php
        $the_query = new WP_Query( $args );
        ?>



        <div class="fr-slider" data-fr='{"dots":false,"infinite":false,"speed":300,"arrows":false,"slidesToShow":4,"slidesToScroll":4,"mobileFirst":false,"responsive":[{"breakpoint":1024,"settings":{"slidesToShow":4,"slidesToScroll":4}},{"breakpoint":600,"settings":{"slidesToShow":3,"slidesToScroll":3}},{"breakpoint":480,"settings":{"slidesToShow":2,"slidesToScroll":1}},{"breakpoint":320,"settings":{"slidesToShow":1,"slidesToScroll":1}}]}'>
            <a href="#" class="arrow prev"><i class="fa fa-angle-left"></i></a>
            <a href="#" class="arrow next"><i class="fa fa-angle-right"></i></a>
            <div class="slides">
                <?php  
                while ( $the_query->have_posts() ) {
                    $the_query->the_post(); ?>




                    <?php if( have_rows('team_members') ): ?>



                            <?php 
                            $x=0;
                            while( have_rows('team_members') ): the_row();

                                // vars
                                $image = get_sub_field('member_photo');
                                $content = get_sub_field('member_bio');
                                $name = get_sub_field('member_name');
                                ?>

                                <div class="slide col-md-2 col-sm-3 col-xs-6">

<div class="slide-inner">
                                    <a id="image" href="#modal_team_member_<?php echo $x ?>" class="open_custom_modal image" >

                                        <img src="<?php echo $image['url']; ?>" class="swatch-img" data-position="top" data-delay="50" data-tooltip="<?php echo $name; ?>" alt="<?php echo $name; ?>" title="<?php echo $name; ?>" width="100" height="100" />

                                    </a>

                                    <a href="#modal_team_member_<?php echo $x ?>" target="_self" class="fl-button-wrap open_custom_modal">
                                        <button class="fl-button btn-trans2 open_custom_modal" role="button"> <span class="fl-button-text">READ BIO</span>
                                        </button>
                                        </a>


</div>

                                    <h5><?php echo $name; ?></h5>
                                    <!-- <div class="bio-content"><?php echo $content; ?></div> -->
                                </div>

                                <?php
                                $x++;
                            endwhile; ?>


                    <?php endif; ?>


                    
                    <?php 
                } ?>
            </div>
        </div>

    <?php wp_reset_postdata(); ?>


    </div>
</div>

<?php /* Modals */
$members=get_field("team_members",$postid);
foreach($members as $k=>$v){
    ?>
    <div class="custom_modal" id="modal_team_member_<?php echo $k ?>">
        <div class="modal_body modal_team_member">
            <a href="#" class="close_modal"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/close-red.png"></a>
            <img src="<?php echo $v["member_photo"]['url']; ?>" class="modal_team_image" />
            <h2><?php echo $v["member_name"]; ?></h2>
            <?php echo $v["member_bio"]; ?>
        </div>
    </div>
<?php } ?>