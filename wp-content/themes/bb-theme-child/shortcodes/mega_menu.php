<div class="sc_mega_menu">
	<div class="sc_overlay"></div>
    <div class="fl-page-nav-wrap">
        <nav class="fl-page-nav navbar navbar-default">
            <?php wp_nav_menu(array("menu"=>$attr["menu"],"menu_class"=>"nav navbar-nav menu","container_class"=>"fl-page-nav-collapse collapse navbar-collapse")); ?>
        </nav>
    </div>
</div>