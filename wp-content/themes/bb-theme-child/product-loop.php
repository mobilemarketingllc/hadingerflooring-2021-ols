<div class="product-grid swatch" itemscope itemtype="http://schema.org/CollectionPage">
    <div class="row">
<?php while ( have_posts() ): the_post(); ?>
    <div class="col-md-3 col-sm-4 col-xs-6">
    <!-- <div class="fl-post-grid-post" itemscope itemtype="<?php //FLPostGridModule::schema_itemtype(); ?>"> -->
    <div class="fl-post-grid-post" itemscope itemtype="Product">
        <?php FLPostGridModule::schema_meta(); ?>
        <?php if(get_field('swatch_image_link')) { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <?php 
                   $image = get_field('swatch_image_link');$height ='600';$width ='600';
 if(strpos($image, 'mmllc-images.s3') !== false || strpos($image, 'products.daltile.com') !== false || strpos($image, 's7.shawimg.com') !== false || strpos($image, 's7.shawfloors.com') !== false || strpos($image, 's7d4.scene7.com') !== false){

        
    if(strpos($image , 's7.shawimg.com') !== false){
        if(strpos($image , 'http') === false){ 
        $image = "http://" . $image;
    }	
       
    }else{
        if(strpos($image , 'http') === false){ 
        $image = "https://" . $image;
    }	
        $image= "https://mm-media-res.cloudinary.com/image/fetch/h_".$height.",w_".$width.",c_limit/".$image."";
    }

  }else{

    $image= 'https://res.cloudinary.com/mobilemarketing/image/upload/c_limit,h_'.$height.',w_'.$width.',q_auto'.$image;

  }	
?>
  <img src="<?php echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
<?php 
                  //  $image = get_field('swatch_image_link');
                   /// echo get_image_url($image,'600','600');  
                    /*if (get_field('brand') == 'Quick-Step' || get_field('brand') == 'Daltile' ) { ?>
                        <img src="<?php the_field('swatch_image_link'); ?>" alt="<?php the_title_attribute(); ?>" />

                    <?php } else { ?>
                        <img src="https://res.cloudinary.com/mobilemarketing/image/upload/h_600,w_600,q_auto/<?php the_field('swatch_image_link'); ?>" alt="<?php the_title_attribute(); ?>" />
                    <?php } */ ?>
                </a>
            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="http://placehold.it/300x300?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                </a>
            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid">
            <h4><?php the_field('collection'); ?></h4>
            <h2 class="fl-post-grid-title" itemprop="headline">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php //the_title(); ?><?php the_field('color'); ?></a>
            </h2>
            <!--
            <a href="<?php //the_permalink(); ?>" target="_self" class="fl-button btn-white" role="button">
                <span class="fl-button-text">VIEW PRODUCT</span>
            </a><br />
            <a class="link" href="<?php //echo site_url(); ?>/coupon/">GET COUPON</a>
            -->
        </div>
    </div>
    </div>
<?php endwhile; ?>
</div>
</div>