<?php



function bbp_hftcustom_register_theme_customizer( $wp_customize ) {


// Add the Header Footer Section to the Customizer.
	$wp_customize->add_section( 'bbp_hftcustom-media', array(
		'title'=> __( 'Header Footer Templates', 'fl-automator' ),
		'description' => __( '
	<b>Instructions</b>
			<ol>
				<li>Create templates for a header/footer.</li>
				<li>Remember to set the background colour & opacity on the row settings of the template.</li>
				<li>Select the Template you wish to insert.</li>
				<li>Choose a Location to apply.</li>
				<li>Edit the page template to "No Header/Footer" if you want to hide it on a specific page.</li>
			</ol>
			<br>
			', 'fl-automator' ),
		'priority'=> 130,
		) );





	$wp_customize->add_setting('separator1', array());
	$wp_customize->add_control('separator1', array(
		'description' => '<hr><h3 style="text-align: center;">	&lt;Header&gt; Settings</h3><p style="text-align: center;">CSS ID of #djcustom-header</p>',
		'section' => 'bbp_hftcustom-media',
		'type' => 'hidden',
		));



//The Header Settings:


	$wp_customize->add_setting('bbp_hftcustom_header_template', array(
		)
	);

	$wp_customize->add_control('bbp_hftcustom_header_template', array(
		'label' => 'Header Template.',
		'section' => 'bbp_hftcustom-media',
		'type' => 'select',
		'default' => 'Choose A Template (Off)',
		'choices' => bbp_hft_get_bb_templates()

		)
	);


	$wp_customize->add_setting('bbp_hftcustom_header_location', array(
		'default' => 'header_none',
		)
	);

	$wp_customize->add_control('bbp_hftcustom_header_location', array(
		'label' => 'Header Location',
		'section' => 'bbp_hftcustom-media',
		'type' => 'select',
		'default' => 'header_none',
		'choices' => array(
			'header_none' => 'Choose A Location',
			'header_above_top_bar' => 'Above Top Bar',
			'header_above_default_header' => 'Above Default Header',
			'header_above_content' => 'Above Content'
			)
		)
	);







   $wp_customize->add_setting('bbp_hftcustom_header_absolute', array(
 		'default' => 'insert_all',
        )
   );

	$wp_customize->add_control('bbp_hftcustom_header_absolute', array(
		'label' => 'Header Insert Type',
		'description' => 'Where and how would you like the header added to your content?',
		'section' => 'bbp_hftcustom-media',
		'default' => 'insert_all',
		'type' => 'select',
		'choices' => array(
			'insert_all' => 'Insert - All Pages',
			'insert_home' => 'Insert - Home Page - (Hide on others)',
			'overlay_all' =>'Overlay - All Pages',
			'overlay_homepage' =>'Overlay - Home Page - (Hide on others)',
			'overlay_homepage_insertrest' =>'Overlay - Home Page - (Insert on others)',
			'fixed_all' =>'Fixed Overlay - All Pages',
			'fixed_homepage' =>'Fixed Overlay - Home Page - (Hide on others)',
			'fixed_homepage_insertrest' =>'Fixed Overlay - Home Page - (Insert on others)',
			),

		)
	);




   $wp_customize->add_setting('bbp_hftcustom_header_opacity', array(
   		'default' => 'false',
        )
   );

	$wp_customize->add_control('bbp_hftcustom_header_opacity', array(
		'label' => 'Header Opacity',
		'description' => 'Force the header background opacity?',
		'section' => 'bbp_hftcustom-media',
		'default' => 'false',
		'type' => 'select',
		'choices' => array(
			'false' => 'Off',
			'homepage' =>'Force on Home Page Only',
			'true' =>'Force on All Pages',
			),
		)
	);



   $wp_customize->add_setting('bbp_hftcustom_header_opacity_alpha', array(
   		'default' => '0.0',
        )
   );

	$wp_customize->add_control('bbp_hftcustom_header_opacity_alpha', array(
		'label' => 'Header Opacity Amount',
		'section' => 'bbp_hftcustom-media',
		'default' => '0.0',
		'type' => 'select',
		'choices' => array (
			'0.0' => '0.0',
			'0.1' => '0.1',
			'0.2' => '0.2',
			'0.3' => '0.3',
			'0.4' => '0.4',
			'0.5' => '0.5',
			'0.6' => '0.6',
			'0.7' => '0.7',
			'0.8' => '0.8',
			'0.9' => '0.9',
			'1.0' => '1.0',
			),
		)
	);








//The Footer Settings:

	$wp_customize->add_setting('separator2', array(
		)
	);
	$wp_customize->add_control('separator2', array(
		'description' => '<br><hr><h3 style="text-align: center;">&lt;Footer&gt; Settings</h3><p style="text-align: center;">CSS ID of #djcustom-footer</p>',
		'section' => 'bbp_hftcustom-media',
		'type' => 'hidden',
		)
	);



	$wp_customize->add_setting('bbp_hftcustom_footer_template', array(
		)
	);

	$wp_customize->add_control('bbp_hftcustom_footer_template', array(
		'label' => 'Footer Template',
		'section' => 'bbp_hftcustom-media',
		'type' => 'select',
		'default' => 'Choose A Template (Off)',
		'choices' => bbp_hft_get_bb_templates()
		)
	);


	$wp_customize->add_setting('bbp_hftcustom_footer_location', array(
		'default' => 'footer_none',
		)
	);

	$wp_customize->add_control('bbp_hftcustom_footer_location', array(
		'label' => 'Footer Location',
		'section' => 'bbp_hftcustom-media',
		'type' => 'select',
		'default' => 'footer_none',
		'choices' => array(
			'footer_none' => 'Choose A Location',
			'footer_above_default' => 'Above the Default Footer', //(also above widgets)
			'footer_below_default' => 'Below the Default Footer'
			)
		)
	);














//The Content Inserter Settings:
	$wp_customize->add_setting('separator3', array(
		)
	);
	$wp_customize->add_control('separator3', array(
		'description' => '<br><hr><h3 style="text-align: center;">Content Inserter Settings</h3><p style="text-align: center;">CSS ID of &lt;div id="djcustom-content"&gt;</p>',
		'section' => 'bbp_hftcustom-media',
		'type' => 'hidden',
		)
	);




	$wp_customize->add_setting('bbp_hftcustom_content_template', array(
		)
	);

	$wp_customize->add_control('bbp_hftcustom_content_template', array(
		'label' => 'Insert A Content Template or Banner',
		'section' => 'bbp_hftcustom-media',
		'type' => 'select',
		'default' => 'Choose A Template (Off)',
		'choices' => bbp_hft_get_bb_templates()
		)
	);


	$wp_customize->add_setting('bbp_hftcustom_content_location', array(
		'default' => 'content_none',
		)
	);

	$wp_customize->add_control('bbp_hftcustom_content_location', array(
		'label' => 'Content Location',
		'description' => 'Where to insert on every page.<br>NOTE - some hooks will only be visible under certain circumstances (eg. fl_comments_open will only display if comments is enabled on a post. fl_header_content_open will only display with default beaver header layout of Nav Bottom, etc.) ',
		'section' => 'bbp_hftcustom-media',
		'type' => 'select',
		'default' => 'content_none',
		'choices' => array(

			'content_none' => 'Choose A Location',
			'fl_before_top_bar' => 'fl_before_top_bar',
			'fl_before_header' => 'fl_before_header',
			'fl_header_content_open' => 'fl_header_content_open',
			'fl_header_content_close' => 'fl_header_content_close',
			'fl_content_open' => 'fl_content_open',
			'fl_post_top_meta_open' => 'fl_post_top_meta_open',
			'fl_post_top_meta_close' => 'fl_post_top_meta_close',
			'fl_post_bottom_meta_open' => 'fl_post_bottom_meta_open',
			'fl_post_bottom_meta_close' => 'fl_post_bottom_meta_close',
			'fl_comments_open' => 'fl_comments_open',
			'fl_comments_close' => 'fl_comments_close',
			'fl_sidebar_open' => 'fl_sidebar_open',
			'fl_sidebar_close' => 'fl_sidebar_close',
			'fl_after_content' => 'fl_after_content',
			'fl_before_footer_widgets' => 'fl_before_footer_widgets',
			'fl_after_footer_widgets' => 'fl_after_footer_widgets',
			'fl_before_footer' => 'fl_before_footer',
			'fl_after_footer' => 'fl_after_footer',
			'fl_page_close' => 'fl_page_close',

			)
		)
	);






	$wp_customize->add_setting('separator4', array(
		)
	);
	$wp_customize->add_control('separator4', array(
		'description' => '<br><br><br><br>',
		'section' => 'bbp_hftcustom-media',
		'type' => 'hidden',
		)
	);
}
add_action( 'customize_register', 'bbp_hftcustom_register_theme_customizer' );